package api;

import model.data_structures.*;
import model.vo.*;

public interface IManejador{

	/**
	 * Carga un archivo con la lista de las peliculas
	 * @param ruta - ruta en el que se encuentra el archivo con la lista de pel�culas
	 */
	public void cargarPeliculas(String ruta);
	
	
	/**
	 * Crea una cola de prioridad de todas las listas asociadas al g�nero entrado por par�metro
	 * @param genero - genero del cual se quieren las pel�culas asociadas
	 * @return Cola de prioridad con las pel�culas asociadas al g�nero
	 */
	public PriorityQueue<VOPreferencia> crearCola(String genero);
	
	/**
	 * Crea un mont�culo de todas las pel�culas asociadas a un g�nero pasado por par�metro
	 * @param genero - G�nero del cual se quieren sus pel�culas asociadas
	 * @return Mont�culo con las pel�culas asociadas
	 */
	public MaxHeapCP<VOPreferencia> crearMonticulo(String genero);
}
