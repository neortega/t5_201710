package model.vo;

import model.data_structures.ILista;

public class VOPelicula {
	
	private long idPelicula; 
	private String titulo;
	private int agnoPublicacion;

	private ILista<String> generosAsociados;
	
	
	
	public long getIdPelicula() {
		return idPelicula;
	}
	public void setIdPelicula(long idPelicula) {
		this.idPelicula = idPelicula;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	
	public int getAgnoPublicacion() {
		return agnoPublicacion;
	}
	public void setAgnoPublicacion(int agnoPublicacion) {
		this.agnoPublicacion = agnoPublicacion;
	}

	public ILista<String> getGenerosAsociados() {
		return generosAsociados;
	}
	public void setGenerosAsociados(ILista<String> generosAsociados) {
		this.generosAsociados = generosAsociados;
	}
	

}
