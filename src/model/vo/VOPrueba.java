package model.vo;

public class VOPrueba implements Comparable<VOPrueba> {

	private String nombre;
	
	private Integer agno;

	@Override
	public int compareTo(VOPrueba object) {
		int respuesta =0;
		if(this.agno>object.agno)
		{
			respuesta =1;
		}
		else if(this.agno<object.agno)
		{
			respuesta = -1;
		}
		else if(this.agno==object.agno)
		{
			respuesta = this.nombre.compareTo(object.nombre);
		}
		// TODO Auto-generated method stub
		return respuesta;
	}
	public Integer getAgno()
	{
		return agno;
	}
	
	public String getNombre()
	{
		return nombre;
	}
	
	public void setAgno(Integer pAgno)
	{
		agno = pAgno;
	}
	
	public void setNombre(String pNombre)
	{
		nombre = pNombre;
	}
}
