package model.vo;

public class VOPreferencia implements Comparable<VOPreferencia> {
	
	private String pelicula;
	
	private int agno;
	
	public String getPelicula()
	{
		return pelicula;
	}
	
	public void setPelicula(String pPelicula)
	{
		pelicula = pPelicula;
	}
	
	public int getAgno()
	{
		return agno;
	}
	
	public void setAgno(int pAgno)
	{
		agno = pAgno;
	}
	@Override
	public int compareTo(VOPreferencia vop) {
		// TODO Auto-generated method stub
	int respuesta = 0;
		
		if(this.agno < vop.agno)
			respuesta = -1;
		else if(this.agno > vop.agno)
			respuesta = 1;
		else
			respuesta = this.pelicula.compareTo(vop.pelicula);
		
		return respuesta;
	}
}
