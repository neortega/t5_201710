package model.logic;

import model.data_structures.*;
import model.vo.*;

public class PruebaColaPrioridad {

	/**
	 * Tiempo que tarda el m�todo max
	 */
	private double tiempoMax;
	
	/**
	 * Tiempo que tarda el m�todo agregar
	 */
	private double tiempoAgregar;
	
	/**
	 * Generador de los datos con los que se probar�n las estructuras de datos
	 */
	public GeneradorDatos generador;
	
	/**
	 * Cola de prioridad a probarse
	 */
	private PriorityQueue<VOPrueba> pqPrueba;
	
	/**
	 * Mont�culo a probarse
	 */
	private MaxHeapCP<VOPrueba> montiPrueba;
	
	/**
	 * Crea una nueva prueba con un nuevo generador y tiempos
	 */
	public PruebaColaPrioridad()
	{
		generador = new GeneradorDatos();
		tiempoMax = 0;
		tiempoAgregar = 0;
	}
	
	

}
