package model.logic;

import model.vo.*;
import model.data_structures.*;

import java.io.*;

import api.*;

public class ManejadorPreferencias implements IManejador{

	private ListaEncadenada <VOPelicula> peliculasList;
	
	private ListaEncadenada<VOGeneroPelicula> generosList;
	
	public ManejadorPreferencias()
	{
		peliculasList = new ListaEncadenada<VOPelicula>();
		generosList = new ListaEncadenada<VOGeneroPelicula>();
	}
	@Override 
	public void cargarPeliculas(String ruta)
	{
		try
		{
			FileReader fr = new FileReader(new File(ruta));
			BufferedReader br = new BufferedReader(fr);
			String linea = br.readLine();
			linea = br.readLine();
			while(linea != null)
			{
				String[] elementos = linea.split(",");
				String[] generos = elementos[2].split("//|");
				
				ListaEncadenada<String> listGen = new ListaEncadenada<String>();
			
				for(int i = 0; i < generos.length; i++)
						listGen.agregarElementoFinal(generos[i]);
				
				
				String titulo = "";
				
				int agno = 0;
				int aux = 0;
				int aux2 = 0;
				
				if(elementos[1].contains("("))
				{
					aux = elementos[1].indexOf("(");
				    aux2 = elementos[1].indexOf(")");
		   
				    String ag = "";
				    
				    for(int i = aux+1; i < aux2+1; i++)
				    {
				    	ag.concat(String.valueOf(elementos[1].charAt(i))); 
				    }
				    
				    agno = Integer.parseInt(ag);
				    
				    String[] titAux = elementos[1].split("(");
				    titulo = titAux[0];
				}
				else
				{
					titulo = elementos[1];
				}
				
				
				VOPelicula vop = new VOPelicula();
				vop.setIdPelicula(Long.parseLong(elementos[0]));
				vop.setTitulo(titulo);
				vop.setAgnoPublicacion(agno);
				vop.setGenerosAsociados(listGen);
				peliculasList.agregarElementoFinal(vop);
				
				ListaEncadenada<VOPelicula> peliculasAsoc = new ListaEncadenada<VOPelicula>();
				VOGeneroPelicula vogp = new VOGeneroPelicula();
				
				NodoSencillo<String> listGenHead = listGen.darCabeza();
				
				if(generosList.darNumeroElementos() == 0)
				{
					while(listGenHead != null)
					{
						vogp.setGenero(listGenHead.darObjeto());
						
						for(int i = 0; i < peliculasList.darNumeroElementos(); i++)
						{
							ListaEncadenada<String> genAsocPeli = (ListaEncadenada<String>)peliculasList.darElemento(i).getGenerosAsociados();
							for(int j = 0; j < genAsocPeli.darNumeroElementos(); j++)
							{
								if(genAsocPeli.darElemento(j).equals(vogp.getGenero()))
								{
									peliculasAsoc.agregarElementoFinal(peliculasList.darElemento(i));
								}
							}
						}
						vogp.setPeliculas(peliculasAsoc);
						generosList.agregarElementoFinal(vogp);
					}
				}
				else
				{
					for(int i = 0; i < generosList.darNumeroElementos()&& listGenHead != null; i++)
					{
						if(generosList.darElemento(i).getGenero().equals(listGenHead.darObjeto()))
							listGenHead = listGenHead.darSiguiente();
						else
							break;
					}
					vogp.setGenero(listGenHead.darObjeto());
					
					for(int i = 0; i < peliculasList.darNumeroElementos(); i++)
					{
						VOPelicula vopAux = peliculasList.darElemento(i);
						ListaEncadenada<String> genAsocPeli = (ListaEncadenada<String>)vop.getGenerosAsociados();
						
						for(int j = 0; j < genAsocPeli.darNumeroElementos(); j++)
						{
							if(genAsocPeli.darElemento(j).equals(vogp.getGenero()))
							{
								peliculasAsoc.agregarElementoFinal(vopAux);
							}
						}
					}
					vogp.setPeliculas(peliculasAsoc);
					generosList.agregarElementoFinal(vogp);
				}
				
				linea = br.readLine();
			}
			br.close();
			fr.close();
		}
		catch(IOException ioe)
		{
			//
		}
	}
	
	@Override
	public PriorityQueue<VOPreferencia> crearCola(String genero)
	{
		PriorityQueue<VOPreferencia> colaPrioridadRespuesta = new PriorityQueue<VOPreferencia>();
		
		ListaEncadenada<VOPelicula> peliAsocGen = new ListaEncadenada<VOPelicula>();
		
		boolean encontrado = false;
		
		if(generosList.darNumeroElementos() == 0)
			return null;
		
		for(int i = 0; i < generosList.darNumeroElementos() && !encontrado; i++)
		{
			VOGeneroPelicula vopg = generosList.darElemento(i);
			
			if(vopg.getGenero().equals(genero))
			{
				peliAsocGen = (ListaEncadenada<VOPelicula>)vopg.getPeliculas();
				encontrado = true;
			}
		}
		
		colaPrioridadRespuesta.maxPQ(peliAsocGen.darNumeroElementos());
		
		VOPreferencia vop = new VOPreferencia();
		
		for(int i = 0; i < peliAsocGen.darNumeroElementos(); i++)
		{
			VOPelicula vopeli = peliAsocGen.darElemento(i);
			
			vop.setPelicula(vopeli.getTitulo());
			vop.setAgno(vopeli.getAgnoPublicacion());
			try
			{
				colaPrioridadRespuesta.insert(vop);
			}
			catch(Exception e)
			{
				//
			}
		}
		
		return colaPrioridadRespuesta;
	}

	@Override
	public MaxHeapCP<VOPreferencia> crearMonticulo(String genero) {
		// TODO Auto-generated method stub
		MaxHeapCP<VOPreferencia> monticuloRespuesta = new MaxHeapCP<VOPreferencia>();
		
		ListaEncadenada<VOPelicula> peliAsocGen = new ListaEncadenada<VOPelicula>();
		
		boolean encontrado = false;
		
		if(generosList.darNumeroElementos() == 0)
			return null;
		
		for(int i = 0; i < generosList.darNumeroElementos() && !encontrado; i++)
		{
			VOGeneroPelicula vopg = generosList.darElemento(i);
			
			if(vopg.getGenero().equals(genero))
			{
				peliAsocGen = (ListaEncadenada<VOPelicula>)vopg.getPeliculas();
				encontrado = true;
			}
		}
		
		monticuloRespuesta.crearCP(peliAsocGen.darNumeroElementos());
		
		VOPreferencia vop = new VOPreferencia();
		
		for(int i = 0; i < peliAsocGen.darNumeroElementos(); i++)
		{
			VOPelicula vopeli = peliAsocGen.darElemento(i);
			
			vop.setPelicula(vopeli.getTitulo());
			vop.setAgno(vopeli.getAgnoPublicacion());
			try
			{
				monticuloRespuesta.agregar(vop);
			}
			catch(Exception e)
			{
				//
			}
		}
		
		return monticuloRespuesta;
	}
}
