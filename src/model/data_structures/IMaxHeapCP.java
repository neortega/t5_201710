package model.data_structures;

public interface IMaxHeapCP <T extends Comparable<T>> {

	/**
	 * Crea una cola de prioridad vac�a con un n�mero m�ximo de elementos
	 * @param max
	 */
	public void crearCP(int max) ;
	/**
	 * Retorna n�mero de elementos en la cola de prioridad
	 */
	public int darNumElementos();
	/**
	 * Agrega un elemento a la cola. Se genera Exception en el caso que se sobrepase el tama�o m�ximo de la cola
	 * @param elemento
	 * @throws Exception
	 */
	public void agregar(T elemento) throws Exception; 
	/**
	 * Saca/atiende el elemento m�ximo en la cola y lo retornar; null en caso de cola vac�a
	 */
	/**
	 * Retorna si la cola es vac�a o no
	 */
	public boolean esVacia();
	/**
	 * Retorna el tama�o m�ximo de la cola
	 */
	public int tamanoMax(); 
	/**
	 * Saca/atiende el elemento m�ximo en la cola y lo retornar; null en caso de cola vac�a
	 * @return
	 */
	public T max();
}
