package model.data_structures;

public class PriorityQueue<T extends Comparable<T>> implements IPriorityQueue<T> {

	/**
	 * Tama�o m�ximo de la cola
	 */
	private int size;
	
	/**
	 * Lista con los elementos de la cola de prioridad
	 */
	private T[] pQ;
	
	@Override
	public void maxPQ(int max) {
		// TODO Auto-generated method stub
		pQ = (T[])new Comparable[max];
		size = max;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return size;
	}

	@Override
	public void insert(T object) throws Exception {
		// TODO Auto-generated method stub
		if(pQ.length != size)
		{
			pQ[pQ.length+1] = object;
		}
		else
			throw new Exception("No se puede agregar el elemento, l�mite alcanzado");
	}

	@Override
	public T max() {
		// TODO Auto-generated method stub
		T elem = null;
		
		if(pQ.length == 0)
			return null;
		
		if(pQ.length == 1)
			return pQ[pQ.length];
		
		for(int i = 0; i < pQ.length; i++)
		{
			if(!(i+1 >= size))
			{
				if(pQ[i].compareTo(pQ[i+1]) > 0)
					elem = pQ[i];
				else
					elem = pQ[i+1];
			}
		}
		
		return elem;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return size == 0;
	}

	@Override
	public int maxSize() {
		// TODO Auto-generated method stub
		return size;
	}

}
