package model.data_structures;


import java.util.Random;

public class GeneradorDatos {

	
	public final static int TAMAGNO_DEFAULT = 10;
	
	/**
	 * Genera una candena de Strings aleatoria con un tama�o pasado por par�metro
	 * @param N - tama�o que tendr� la cadena
	 * @return Cadena de caracteres con el tama�o predefinido
	 */
	public String[] generarCadenas(int N)
	{
		String[] respuesta = new String[N];
		for (int i = 0; i < respuesta.length; i++) {
			respuesta[i] = getCadenaAlfanumAleatoria(); 
		}
		return respuesta;

	}
	
	/**
	 * Genera una cadenada de tama�o N de a�os aleatorios que esten entre 1950 y 2017
	 * @param N - N�mero que definir� el tama�o del arreglo de a�os
	 * @return Arreglo de a�os con el tama�o predefinido
	 */
	public Integer[] generarAgnos(int N)
	{
		Integer[] respuesta = new Integer[N];
		int numero = (int )Math.random()*60 +1;
		for (int i = 0; i < respuesta.length; i++) {
			respuesta[i] = numero+1950;
		}
		return respuesta;
	}
	
	/**
	 * M�todo que crea una cadena de caracteres con contenido aleatorio y tama�o predefinido (m�ximo 10 caracteres)
	 * @return Un string con caracteres aleatorios
	 */
	public String getCadenaAlfanumAleatoria(){
		String cadenaAleatoria = "";
		long milis = new java.util.GregorianCalendar().getTimeInMillis();
		Random r = new Random(milis);
		int i = 0;
		while ( i < TAMAGNO_DEFAULT){
			char c = (char)r.nextInt(255);
			if ( (c >= '0' && c <='9') || (c >='A' && c <='Z') )
			{
				cadenaAleatoria += c;
			i ++;
			}
		}
		return cadenaAleatoria;
	}
}
