package model.data_structures;

public class MaxHeapCP <T  extends Comparable<T>> implements IMaxHeapCP<T> {


	private T[] array;

	private int length;
	
	public void crearCP(int max) {
		
		array = (T[])new Object[max+1];
		length = 0;
	}

	@Override
	public int darNumElementos() {
		return length;
	}

	@Override
	public void agregar(T elemento) throws Exception {
		length++;
		array[length] = elemento;
		swim(length);
	}

	@Override
	public boolean esVacia() {
		return length==0;
	}

	@Override
	public int tamanoMax() {
		
		return array.length;
	}
	private void swim (int k)
	{
		while (k> 1 && array[k/2].compareTo(array[k])<0)
		{
			T elem = array[k];
			T elem2 = array[k/2];
			array[k] = elem2;
			array[k/2] = elem;
			k = k/2;
		}
	}
	
	private void sink(int k)
	{
		while(2*k <= length)
		{
			int j = 2*k;
			if(j< length && array[j].compareTo(array[j+1])<0 ) j++;
			if (array[j].compareTo(array[j+1])>0) break;
			T elem = array[k];
			T elem2 = array[j];
			array[k] = elem2;
			array[j] = elem;
			k=j;
		
		}
	}

	@Override
	public T max() {
		if(length==0)return null;
		else
		{
			T elem = array[length];
			T elemBuscado = array[1];
			array[length] = elemBuscado;
			array[1] = elem;
			length--;
			sink(1);
			return elemBuscado;
		}
	}
}
