package model.data_structures;

public interface IPriorityQueue <T extends Comparable<T>> {

	/**
	 * Crea una cola de prioridad vac�a con un n�mero m�ximo de elementos
	 * @param max - Cantidad de elementos m�ximos que tendr� la cola
	 */
	public void maxPQ(int max);
	
	/**
	 * Retorna el n�mero de elementos en la cola de prioridad
	 * @return N�mero actual de elementos en la cola de prioridad
	 */
	public int size();
	
	/**
	 * Agrega un objeto a la cola de prioridad. Se genera excepci�n si la cantidad de elementos sobre pasa el m�ximo de la cola.
	 * @param object - Objeto a agregar
	 * @throws Exception - Lanza excepci�n si se sobrepasa el m�ximo de elementos en la cola de prioridad
	 */
	public void insert(T object) throws Exception;
	
	/**
	 * Retorna/atiende el elemento m�ximo en la cola y lo retorna 
	 * @return T - Objeto m�ximo en la cola
	 * @return null - Si la cola est� vac�a
	 */
	public T max();
	
	/**
	 * Retorna si la cola es vac�a o no
	 * @return true Si la cola es vac�a
	 * @return false Si la cola no est� vac�a
	 */
	public boolean isEmpty();
	
	/**
	 * Retorna el tama�o m�ximo de la cola
	 * @return Tama�o m�ximo que permite la cola de prioridad
	 */
	public int maxSize();
	
}
